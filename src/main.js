import Vue from 'vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

import axios from 'axios'
import VueAxios from 'vue-axios'

import vSelect from 'vue-select'

import App from './App.vue'

Vue.use(VueAxios, axios)
Vue.use(BootstrapVue,IconsPlugin)
Vue.component('v-select', vSelect)

Vue.config.productionTip = false

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'vue-select/dist/vue-select.css';

export const eventBus = new Vue();

new Vue({
  render: function (h) { return h(App) },
}).$mount('#app')
