## archistar-frontend-task

### Environment expected

Node v10.21.0 (npm v6.14.4)

### Project setup
```
npm install
```

#### Compiles and hot-reloads for development
```
npm run serve
```

#### Compiles and minifies for production
```
npm run build
```